var undefined
	, e='env'
	, o={
		sioport:443
	}
	, Web = function(options){ 
			this.options = options;
			for(setting in options) this[setting] = options[setting];
			this.address = this.address || '0.0.0.0';
			this.wwwport = this.wwwport || 80;
			this.sioport = this.sioport || 8080;
			this.express = require('express');
			this.mongo   = require('mongoskin');
			this.email   = require("emailjs").server;
			this.uneval  = require('node-uneval');
			var web        = this,
					SIO        = require('socket.io')
				, MongoStore = require('connect-mongo')(this.express);
			this.store     = new MongoStore({db:'sessions'});
			this.sessions  = this.mongo.db('mongo://localhost:27017/sessions?auto_reconnect=true').collection('sessions');
			this.cookie    = require('./node_modules/express/node_modules/connect').utils.parseCookie;
			this.core      = this.express.createServer();
			this.core.web  = this; 
			this.sio       = SIO.listen(this.sioport);
			this.sio.configure(function (){
				web.sio.set('authorization', function (handshake, accept) {
					if (handshake.headers.cookie){
						handshake.cookie = web.cookie(handshake.headers.cookie);
					} else {
						accept('nocookie',null); 
						return
					}
					web.sessions.findOne(
						{ _id:handshake.cookie['connect.sid'] },
						function(err,ses){
							if(!err){
								handshake.session = ses;
								accept(null, true);
								console.log('[ '+ses._id+' ] - socket.io accept')
							}
						}
					) // call 
				})
				web.sio.set('log level', 1);
				console.log('socket.io configure')
			})
			this.core.configure('production' , this.ecfg(this.zone));
			this.core.listen(this.wwwport, this.initlog());
		}
	, Core = { 
				vhcfg: {
//					'elser'       :	{ root: 'elser/www/'   			},
//					'zhidenko'    :	{ root: 'zhidenko/www/'   	},
//					'elenshleger' : { root: 'elenshleger/www/'	},
//					'terezanov'   :	{ root: 'terezanov/www/'		},
					'sochidetox'  : { root: 'sochidetox/www/'		}
				}
			, root		: './'
			, app			: 'vhost.js'//'web.app.js'
			, zone		: '.evm'
			, initmsg	: 'web.core server listening on port %d in %s mode'
			, db			: function(){
					var web = this;
					return function(db){
						if(!db.name) return null;
						if(!db.passwd || !db.user) db.user = db.passwd = '';
						else { db.user = db.user + ':'; db.passwd = db.passwd + '@'; }
						db.server = db.server || 'localhost:'
						db.port = db.port || '27017/';
						return web.mongo.db(
							'mongo://' + db.user + db.passwd + db.server + db.port + db.name + "?auto_reconnect=true"
						)
					}
				}
			, websocket	: function(ns){
					var web = this;
					return function(callback){
						var vhns = web.sio.of('/'+ns);
						vhns.on( 'connection', function(socket){ 
							socket.session = {
								get:function(cb){
									if(typeof cb != 'function') return;
									cb(socket.handshake.session)
								},
								set:function(sid, data, cb){
									if(!sid || typeof data != 'object' || typeof cb != 'function') return;
									web.sessions.update({_id:sid},{$set:{siodata:data}}, {upsert:false, multi:false, safe:true}, cb)
								}
							}
							callback(socket) 
						});
					}
				}
			,	initlog		: function(){
					var web = this;
					return function(){ 
						console.log( web.initmsg, web.core.address().port, web.core.settings.env ) 
					}
				}
			,	ecfg			: function(zone){
					var web = this;
					for(vh in web.vhcfg) web.vhcfg['www.'+vh] = web.vhcfg[vh]
					return function(){
						for(vh in web.vhcfg){
							if(typeof  web.vhcfg[vh] == 'object'){
								web.vhcfg[vh].root 		= web.vhcfg[vh].root || 'localhost';
								web.vhcfg[vh].port 		= web.wwwport;
								web.vhcfg[vh].host 		= vh;
								web.vhcfg[vh].zone	 	= zone;
								web.vhcfg[vh].sioport = web.sioport;
								web.vhcfg[vh].sio 		= web.sio;
								web.vhcfg[vh].options = web.options;
								web.vhcfg[vh].express = web.express;
								web.vhcfg[vh].wsocket = web.websocket(vh);//wsocket;
								web.vhcfg[vh].parse 	= web.parse;
								web.vhcfg[vh].store 	= web.store;
								web.vhcfg[vh].email 	= web.email;
								web.vhcfg[vh].db 		  = web.db();
								web.vhcfg[vh].createServer = function(){
									var app = web.express.createServer()
								}
								web.core.use(
									web.express.vhost(
										vh+zone, 
										require( web.root + web.vhcfg[vh].root + web.app ).start( web.vhcfg[vh] )
									)
								)
								//console.dir(web.core)
							} 							
						};web.core.use(web.express.errorHandler({ dumpExceptions: true, showStack: true })); 
					}
				}
			, parse:function(tags){
					if(!tags[0]) return "parse error";
					var attr, idx, part, tagname = tags[0], markup = "<"+tagname+" ", tagclose = "></"+tagname+">";
					if(tags[1]){
						for(attr in tags[1]) if(tags[1].hasOwnProperty(attr)){
							markup += attr + '="' + tags[1][attr] + '" '
						}
					}
					if(tags.length > 2){
						markup += ">";tagclose = "</"+tagname+">"
						for(idx = 2; idx < tags.length; idx++){
							if(typeof tags[idx] == "string") markup += tags[idx]; 
							else if(typeof tags[idx] == "function") { part = tags[idx](tags); if(typeof part == "string") markup += part; }
							else if(tags[idx] instanceof Array) markup += this.parse(tags[idx]);
							else if(typeof tags[idx] == "object"){
								if(tags[idx].script) markup += this.uneval(tags[idx].script)
							}
						}
					}
					markup += tagclose;
					return markup
				}
		};
Web.prototype = Core;
web = new Web(o);
module.exports = web.core;
