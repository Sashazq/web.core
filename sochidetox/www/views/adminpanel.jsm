var fs = require('fs');
module.exports.jstemplate = function(script, template){
	function toString(o){
		if(o instanceof Array) {
			var s='[';
			for(e=0; e<o.length; e++){
				if(s!='[')s+=',';
				s+=toString(o[e])
			}
			return s+']';
		} else if( (typeof o == 'function') || (typeof o == 'number') || (typeof o == 'boolean') ) {
			return o.toString();
		} else if(typeof o == 'string') {
			return '"'+o.toString()+'"';
		} else {
			var s='{'
			for(f in o){
				if(s != '{') s+=',';
				s+='"'+f+'"'+':'+toString(o[f])
			}
			return s+'}'
		}
	}
	var rg = new RegExp(/::[^:]*::/gm), jstpl = [];
	while((s = rg.exec(script)) != null){ jstpl[s[0]]=0 }
	for(tpl in jstpl){
		rg = new RegExp(tpl, 'gm')
		script = script.replace( rg, toString(eval( tpl.split('::').join('') )) )
	}
	return script
}
module.exports.script = fs.readFileSync(__dirname+'/adminpanel.js').toString();
module.exports.content = 
['div', { id:"adminpanel", class:"abs closed"},
	['div', { id:"profile" },
		['div', { id:"savepage", 							class:"toolbutton", 						title:"Сохранить страницу"}],
		['div', { id:"escapechanges", 				class:"toolbutton", 						title:"Вернуть к последнему сохранению"}],
		['div', { id:"undo", 									class:"toolbutton editbutton", 	title:"Отмена"}],
		['div', { id:"redo", 									class:"toolbutton editbutton", 	title:"Возврат"}],
		['div', { id:"logout", 								class:"toolbutton", 						title:"Выйти из системы"}],
		['div', { id:"dockpanel", 						class:"toolbutton", 						title:"Показать/скрыть панель управления"}],
		['div', { class:"clear"}]
	],
	['div', { id:"tools"},
		['div', { id:"bold", 									class:"toolbutton editbutton", 	title:"Жирный"}],
		['div', { id:"italic", 								class:"toolbutton editbutton", 	title:"Курсив"}],
		['div', { id:"strikeThrough", 				class:"toolbutton editbutton", 	title:"Зачеркнутый"}],
		['div', { id:"underline", 						class:"toolbutton editbutton", 	title:"Подчеркнутый"}],
		['div', { class:"clear"}],
		['select',{id:"fontface"},
			['option',{},"Arial"],
			['option',{},"Courier"],
			['option',{},"Times New Roman"]
		],
		['select',{id:"fontsize"},
			['option',{},"8"],
			['option',{},"10"],
			['option',{},"12"],
			['option',{},"14"],
			['option',{},"16"],
			['option',{},"18"],
			['option',{},"20"]
		],
		['div', { id:"subscript", 						class:"toolbutton editbutton", 	title:"Подстрочный"}],
		['div', { id:"superscript", 					class:"toolbutton editbutton", 	title:"Надстрочный"}],
		['div', { class:"clear"}],
		['div', { id:"justifyFull", 					class:"toolbutton editbutton", 	title:"По ширине"}],
		['div', { id:"justifyLeft", 					class:"toolbutton editbutton", 	title:"По леаому краю"}],
		['div', { id:"justifyCenter", 				class:"toolbutton editbutton", 	title:"По центру"}],
		['div', { id:"justifyRight", 					class:"toolbutton editbutton", 	title:"По правому краю"}],
		['div', { class:"clear"}],
		['div', { id:"indent", 								class:"toolbutton editbutton", 	title:"+ Отступ >"}],
		['div', { id:"outdent", 							class:"toolbutton editbutton", 	title:"- Отступ <"}],
		['div', { id:"insertorderedlist", 		class:"toolbutton editbutton", 	title:"Нумерованный список"}],
		['div', { id:"insertunorderedlist", 	class:"toolbutton editbutton", 	title:"Маркированный список"}],
		['div', { class:"clear"}],
		['div', { id:"foreColor", 						class:"toolbutton", 	title:"Цвет текста"},
			(function(c,r){
				var cp = ['div',{class:"abs colorpicker hidden"}]
				for(var y=0;y<r;y++ ){
					for(var x=0;x<c;x++ ){
						cp.push(['div',{class:"colorsample c"+y+x}])
					}					
					cp.push(['div',{class:"clear"}])
				}
				return cp;
			})(8,5)
		],
		['div', { id:"backColor", 						class:"toolbutton", 	title:"Цвет фона"},
			(function(c,r){
				var cp = ['div',{class:"abs colorpicker hidden"}]
				for(var y=0;y<r;y++ ){
					for(var x=0;x<c;x++ ){
						cp.push(['div',{class:"colorsample c"+y+x}])
					}					
					cp.push(['div',{class:"clear"}])
				}
				return cp;
			})(8,5)
		],
		['div', { id:"createlink", 						class:"toolbutton no-save-selection", 	title:"Создать ссылку"},
			['div', {class:'abs linkform hidden'},
				['input', {id:'linkurl', type:'text', name:'linkurl'}],['div',{id:'linkapply', class:'toolbutton no-save-selection', title:'Применить'}]
			]
		],
		['div', { id:"unlink",		 						class:"toolbutton editbutton", 	title:"Убрать ссылку"}],
		['div', { class:"clear"}],
		['div', { id:"insertHorizontalRule", 	class:"toolbutton editbutton", 	title:"Горизонтальная линия"}],
		['div', { id:"insertImage", 					class:"toolbutton", 	title:"Вставить изображение"}],
		['div', { id:"floatLeft", 						class:"toolbutton", 	title:"Обтекание справа"}],
		['div', { id:"floatRight", 						class:"toolbutton", 	title:"Обтекание слева"}],
		['div', { class:"clear"}],
		['div', { class:"clear"}],
		
	]
]