var puc = module.parent.exports.puc
	, banners = [
		{caption:'ВИЛЛА', src:'/img/apparts/villa.png', href:puc('/prozhivanie/nomera/nomer-villa')},
		{caption:'НОМЕР «ДЖЕНТЛЬМЕНСКИЙ»', src:'/img/apparts/gentlemen.png', href:puc('/prozhivanie/nomera/nomer-djentelmenskiy')},
		{caption:'НОМЕР «ЗЕЛЕНЫЙ»', src:'/img/apparts/green.png', href:puc('/prozhivanie/nomera/nomer-zeleniy')},
		{caption:'НОМЕР «ГЛАМУР»', src:'/img/apparts/glamur.png', href:puc('/prozhivanie/nomera/nomer-glamur')},
		{caption:'НОМЕР «РОЗЫ В ШОКОЛАДЕ»', src:'/img/apparts/roseinchocolate.png', href:puc('/prozhivanie/nomera/rozy-v-shocolade')},
		{caption:'НОМЕР «БРИТАНСКИЙ»', src:'/img/apparts/british.png', href:puc('/prozhivanie/nomera/nomer-britanskiy')},
		{caption:'НОМЕР «ФИОЛЕТОВЫЙ ЛЕС»', src:'/img/apparts/violeteforest.png', href:puc('/prozhivanie/nomera/nomer-fioletovyj-les')},
		{caption:'НОМЕР «ГОЛУБОЙ ЛЕС»', src:'/img/apparts/blueforest.png', href:puc('/prozhivanie/nomera/nomer-goluboy-les')},
		{caption:'НОМЕР «БАРХАТНЫЙ»', src:'/img/apparts/velvet.png', href:puc('/prozhivanie/nomera/nomer-barhatniy')},
		{caption:'НОМЕР «ЗОЛОТОЙ»', src:'/img/apparts/gold.png', href:puc('/prozhivanie/nomera/nomer-zolotoy')},
		{caption:'НОМЕР «ФРАНЦУЗСКИЙ»', src:'/img/apparts/france.png', href:puc('/prozhivanie/nomera/nomer-frantsuzskiy')}
	]
module.exports.content = 
['div',{id:'content'},
	['div', {class:"text"},
		'<h1>Номера</h1><hr/>',
		'<p>Мы предлагаем нашим гостям проживание в удобных и роскошных номерах. Все они выполнены по индивидуальным дизайнерским проектам и отличаются богатством, красотой и изяществом. </p>',
		'<p>Номера в бутик-отеле Mishilen Detox & Wellness оборудованы по последнему слову техники: в них есть все необходимое для комфортного отдыха - современные пластиковые окна, теплый пол, система кондиционирования и вентиляции, стильные дизайнерские люстры и светильники, высококачественная сантехника, элитное сатиновое постельное белье, богатая библиотека и видеотека, бесплатный WiFi. Каждый номер также оснащен индивидуальным сейфом,  феном, телевизором. Уборка номеров проводится ежедневно. Стильно оформленный интерьер удивит даже самых взыскательных гостей.</p>',
		'<p>В наличии имеются двухместные номера, а также номера для семейного проживания. </p>',
		'<p><b>Узнать самые выгодные цены на проживание в и забронировать номер можно по телефону горячей линии 8 (495) 788-34-94. Звонки принимаются круглосуточно!</b></p>',
		(function(){
			var m = ['div',{class:'banners'}]
			for(idx=0; idx < banners.length; idx++ ){
				m.push(
					['div',{class:"bannerlink hoverable"},
						['a', {href:banners[idx].href},
							['div',{class:"banner onhover normal"},
								['img',{src:banners[idx].src}]
							],
							['div',{class:"capt"},
								banners[idx].caption
							],
							['div', {class:'abs bannershadow onhover normal'}]
						]
					]
				)
			}
			console.log(m);
			return m;
		})()
	]
]
// style="text-decoration:underline;"