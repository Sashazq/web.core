module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
var fs = module.parent.exports.fs;
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			cnt = fs.readdirSync(__dirname+'/../../public/img/restoran').length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/restoran/'+i+'.png'}])
			return frama;
		})(),
		'<h1>Ресторан</h1>',
		'<hr/>',
		'<p>Ресторан в бутик-отеле Mishilen Detox & Wellness не оставит равнодушным никого. Вам понравятся изысканный европейский интерьер, повара, специализирующиеся на приготовлении диетического питания и внимательные официанты.</p>',
		'<p>В ресторане бутик-отеля Mishilen Detox & Wellness Вас ждет более 50 видов минеральной и лечебной воды, пятиразовое раздельное питание, подобранное индивидуально для каждого гостя с учетом особенностей организма и вкусовых привычек. При этом Вам не придется голодать: организм будет получать все необходимые питательные вещества в полном объеме! Мы используем только высококачественные экологически чистые продукты и свежайшие овощи, фрукты, ягоды.</p>',
		'<p>Наше меню способно удовлетворить даже самого взыскательного гурмана – и при этом оно будет сбалансированным и полезным для здоровья.</p>',
		'<p>Современная диетическая кухня может быть вкусной и красиво поданной. Убедитесь в этом сами!</p>'
	]
]
// style="text-decoration:underline;"