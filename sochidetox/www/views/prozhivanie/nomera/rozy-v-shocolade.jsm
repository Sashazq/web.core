var fs = module.parent.exports.fs
	, appartname = 'roseinchocolate';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/'+appartname+'/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Розы в шоколаде»</h1>',
		'<hr/>',
		'<p>Идеален для тех, кто мечтает о роскоши, имеет неповторимый дизайн. Подходит как для индивидуального, так и для семейного отдыха. В номере широкая удобная кровать.</p>',
		'<p>Также имеется терраса, на которой можно загорать днем (для этого на террасе есть шезлонги) и наслаждаться красивым пейзажем вечером. С террасы открывается вид на море и подножье горы Ахун.</p>',
		'<p>Ванная комната оснащена душевой кабиной, ванной. Зеркало и раковина в ванной комнате изготовлены по индивидуальному заказу.</p>',
		'<p>В номере также имеется халат, тапочки, фен, телевизор, кондиционер, бесплатный Wi-Fi.</p>'

	]
]
// style="text-decoration:underline;"