var fs = module.parent.exports.fs
	, appartname = 'violeteforest';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/'+appartname+'/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Фиолетовый лес»</h1>',
		'<hr/>',
		'<p>Номер выдержан в благородном фиолетовом цвете, что делает его очень уютным и стильным. Цветочные орнаменты добавляют в интерьер изящество и легкость. Из окна номера Вы сможете насладиться красивейшим видом на национальный заповедник горы Ахун. В номере большая широкая кровать, халат, тапочки, телевизор, кондиционер, бесплатный Wi-Fi.</p>',
		'<p>Ванная комната оборудована душевой кабиной, всеми необходимыми ванными принадлежностями, полотенцами, феном.</p>',
		'<p>Номер предназначен для некурящих гостей.</p>'
	]
]
// style="text-decoration:underline;"