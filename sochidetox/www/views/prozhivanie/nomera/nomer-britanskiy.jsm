var fs = module.parent.exports.fs
	, appartname = 'british';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/british/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Британский»</h1>',
		'<hr/>',
		'<p>Номер выполнен в традиционном английском стиле, что позволяет ощутить себя на отдыхе в старой доброй Англии. Особенностью данного номера является шахматный стол, что великолепно подойдет для проведения досуга. Также имеется уютная терраса, с которой открывается превосходный вид на море и заповедник горы Ахун.</p>',
		'<p>Ванная комната оборудована душевой кабиной и всем необходимым – ванными принадлежностями, полотенцами, феном, халатом и тапочками.</p>',
		'<p>В номере имеется большая кровать, телевизор, кондиционер, бесплатный Wi-Fi</p>'
	]
]
// style="text-decoration:underline;"