var fs = module.parent.exports.fs
	, appartname = 'glamur';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/glamur/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Гламур»</h1>',
		'<hr/>',
		'<p>Номер «Гламур» восхитит Вас своей роскошью и неповторимым дизайном. В интерьере преобладает благородный цвет спелой сливы, обои напоминают кружево.</p>',
		'<p>Этот номер идеально подходит для молодых семейных пар. Большая удобная ванна придаст отдыху дополнительный комфорт. Она оборудована огромной ванной-джакузи и всем необходимым – ванными принадлежностями, феном, полотенцами, халатом, тапочками.</p>',
		'<p>Номер «Гламур» имеет выход на террасу, откуда можно наслаждаться великолепным видом на море, а также загорать, не выходя из номера. Вечерами на террасе очень приятно посидеть за чашкой чая.</p>',
		'<p>В номере имеется телевизор, кондиционер, бесплатный Wi-Fi.</p>'
	]
]
// style="text-decoration:underline;"