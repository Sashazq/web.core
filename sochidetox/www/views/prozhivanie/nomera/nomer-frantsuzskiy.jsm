var fs = module.parent.exports.fs
	, appartname = 'france';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/'+appartname+'/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Французский»</h1>',
		'<hr/>',
		'<p>Номер выполнен в стиле традиционного французского загородного дома, в интерьере преобладают белый и голубой цвета. Идеален для семейного отдыха с детьми. Имеет две спальни, выполненные в разных стилях, и две ванные комнаты.</p>',
		'<p>Ванная комната оборудована душевой кабиной, в ней есть все необходимые ванные принадлежности, полотенца, халаты, тапочки, фен.</p>',
		'<p>В номере имеются телевизор, кондиционер, бесплатный Wi-Fi. Из окна открывается великолепный вид на гору Ахун. </p>'
	]
]
// style="text-decoration:underline;"