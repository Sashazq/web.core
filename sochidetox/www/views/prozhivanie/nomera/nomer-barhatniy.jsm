var fs = module.parent.exports.fs
	, appartname = 'velvet';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/'+appartname+'/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Бархатный»</h1>',
		'<hr/>',
		'<p>Комфортный и удобный номер в европейском стиле. В интерьере преобладают темные, пастельные тона и бархат. Отлично подходит и для семейного, и для индивидуального отдыха. Номер «Бархатный» очень комфортный и уютный, из окна открывается великолепный панорамный вид на море и подножье горы Ахун.</p>',
		'<p>Ванная комната оборудована душевой кабиной, санузлом с биде. Зеркало и раковина в ванной комнате изготовлены по индивидуальному заказу.</p>',
		'<p>В номере имеются:</p>',
		'<ul>',
		'<li>телевизор,</li>',
		'<li>кондиционер,</li>',
		'<li>внутренний телефон</li>',
		'<li>бесплатный Wi-Fi</li>',
		'<li>тапочки,</li>',
		'<li>халаты,</li>',
		'<li>фен.</li>',
		'</ul>',
		'<p>Номер предназначен для некурящих гостей.</p>'
	]
]
// style="text-decoration:underline;"