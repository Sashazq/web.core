var fs = module.parent.exports.fs
	, appartname = 'blueforest';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/'+appartname+'/'+i+'.png'}])
			return frama;
		})(),
		'<h1>номер «Голубой лес»</h1>',
		'<hr/>',
		'<p>Это роскошный номер выполнен в бело-голубой гамме. Из окна открывается красивый вид на национальный парк. Номер оборудован двумя односпальными кроватями, имеются халаты и тапочки. В ванной комнате есть душевая кабина, все необходимые ванные принадлежности, полотенца, фен.</p>',
		'<p>В номере имеется телевизор, кондиционер, бесплатный Wi-Fi.</p>',
		'<p>Номер предназначен для некурящих гостей.</p>'
	]
]
// style="text-decoration:underline;"