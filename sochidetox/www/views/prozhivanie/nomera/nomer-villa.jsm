var fs = module.parent.exports.fs
	, appartname = 'villa';
module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			imgdir = __dirname.replace('/views/prozhivanie/nomera', '/public/img/apparts'),
			cnt = fs.readdirSync(imgdir+'/'+appartname).length;
			for(i=1;i<cnt;i++) frama.push(['img',{src:'/img/apparts/villa/'+i+'.png'}])
			return frama;
		})(),
		'<h1>Вилла</h1>',
		'<hr/>',
		'<p>Вилла – поистине роскошные апартаменты, способные удовлетворить самого капризного гостя. Это отдельно стоящее здание, где Вы можете насладиться настоящим семейным отдыхом. К Вашим услугам две удобные спальни, шикарная гостиная с камином и огромным диваном. Каждая из спален имеет свою отдельную ванную комнату. В ванных комнатах имеются биде, все необходимые ванные принадлежности, полотенца, фены.</p>',
		'<p>Все номера на втором этаже оснащены балконом , с которого можно наблюдать шикарный вид на море, на террасу отеля и бассейн.</p>',
		'<p>Вилла выполнена в классическом европейском стиле, в оформлении интерьера использованы благородные цвета и золотые элементы декора.</p>',

	]
]
// style="text-decoration:underline;"