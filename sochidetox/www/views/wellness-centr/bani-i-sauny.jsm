module.exports.content = 
['div',{id:'content'},
	['div', {class:"text"},
		['div', {class:"frameimg", style:"width:634px;margin-left:auto;margin-right:auto;"},
			['img', {style:'margin-bottom:-4px;', src:"/img/pages/frame/1.png"}],['img', {style:'float:left;', src:"/img/pages/frame/2.png"}],
			['img', {style:'float:left;', src:"/img/pages/wellness/sauna/1.png"}],
			['img', {src:"/img/pages/frame/3.png"}],['img', {style:'margin-top:-4px;', src:"/img/pages/frame/4.png"}]
		],
		'<h1>Бани и сауны</h1>',
		'<hr/>',
		'<p>Тепловое воздействие оказывает благотворное воздействие на человеческий организм. Под воздействием тепла кровь начинает двигаться быстрее, открываются поры, происходит интенсивное потоотделение. Вместе с потом мы избавляемся от лишнего жира, шлаков, кислот, токсинов и других вредных веществ, поэтому без тепловых процедур эффективность детокс-программы будет недостаточной.</p>',
		'<p>В Mishilen Detox & Wellness мы предлагаем нашим гостям самые эффективные процедуры.</p>',
		'<p><b>Инфракрасная сауна</b></p>',
		'<p>Инфракрасная сауна сильно от отличается от сауны традиционной. Она представляет собой небольшую кабину, оборудованную специальными циркониево-керамическими инфракрасными излучателями. В  такой сауне нет печи, ее не нужно нагревать и протапливать. Инфракрасное излучение воздействует непосредственно на тело, не нагревая воздух, поэтому температура в такой сауне очень комфортная. </p>',
		'<p>Уникальное свойство ИК лучей – это способность проникать в тело на глубину до 4 сантиметров. Такой глубокий прогрев хорошо действует на кожу, способствует расслаблению мышц. За счет сильного прилива крови ускоряются обменные процессы, ткани насыщаются кислородом. В ИК-сауне человек обильно потеет, что способствует выходу вредных веществ из организма, очищению и оздоровлению кожи. Температура тела во время процедуры поднимается до 38 градусов: благодаря этому организм активно борется с вирусами и инфекциями. ИК-сауну можно использовать как для лечения, так и для профилактики различных вирусных заболеваний.</p>',
		'<p><i >Эффект.</i> По сравнению с традиционной сауной, где тепло идет через воздух, в инфракрасной кабине не происходит поверхностного нагрева тела: процедуру очень легко проходят даже тем, кто плохо переносит жару. ИК-лучи являются одним из самых мягких и бережных видов воздействий, они не приносят вреда организму. Зато польза от такой процедуры очевидна:</p>',
		'<ul>',
		'<li>избавление от шлаков, токсинов и др. вредных веществ</li>',
		'<li>похудение, избавление от целлита и отеков</li>',
		'<li>оздоровление организма, профилактика инфекционных и вирусных заболеваний</li>',
		'<li>снятие избыточного напряжения с мышц, избавление от головных болей</li>',
		'<li>насыщение тканей кислородом, улучшение работы всех систем организма</li>',
		'<li>улучшение внешнего вида кожи</li>',
		'<li>улучшение настроения, состояние легкости</li>',
		'</ul>',
		'<p><b>Кедровая бочка</b></p>',
		'<p>Насладиться чудесными ароматами трав и почувствовать себя частью природы можно во время сеанса в кедровой бочке. Баня является обязательной частью программы по очищению организма. Действие банных процедур на организм человека уникально: оно дает очень мощный детокс-эффект и невероятный заряд бодрости. Недаром ощущения после бани описывают как второе рождение. «Заново родиться» гостям Mishilen Detox & Wellness поможет кедровая бочка – своего рода баня в миниатюре.</p>',
		'<p>Кедровая бочка по внешнему виду действительно напоминает бочку. Ее размер рассчитан на то, чтобы внутри на специальном сидении мог комфортно разместиться человек. Голова пациента во время процедуры находится над бочкой и не подвергается действию температуры и пара, поэтому переносить сеанс легко и комфортно. Посещать кедровую бочку можно даже тем людям, кому запрещены традиционные бани и сауны.</p>',
		'<p>В конструкцию бочки встроены парогенераторы, благодаря которым бочка изнутри прогревается и заполняется влажным горячим паром. Для лучшего эффекта используют различные целебные травы, фитосборы, эфирные масла. Немалую роль играет и кедр, из которого изготовлена сама бочка: он также обладает множеством полезных свойств.</p>',
		'<p><i >Эффект.</i> В кедровой бочке происходит глубокий и равномерный нагрев тела, вследствие чего обменные процессы в тканях ускоряются, уходит лишняя жидкость – основная причина целлюлита. Кожа подтягивается и разглаживается – это можно заметить уже после 1-2 процедур в кедровой бочке. Травы и эфирные масла помогают организму вывести шлаки и токсины, раскрыть и очистить поры. </p>',
		'<p>Кедровая бочка отлично снимает нервное напряжение, расслабляет мышцы, нормализует сон. После курса таких процедур Вы забудете о головных болях и депрессии: под действием тепла в организме начинает вырабатываться гормон радости – эндорфин. </p>',
		'<p>Вдыхая ароматы целебных трав и кедра, Вы забудете о своих заботах и тревогах! </p>',
		'<p><b>Тепловые процедуры – различные бани и сауны – это отличное сочетание пользы и удовольствия. Вы сможете по-настоящему расслабиться, оздоровить и очистить свой организм, сбросить груз стрессов и переживаний.</b> Чувство легкости – и моральной, и физической, которое возникает после посещения парной, несравнимо ни с чем! </p>'
	]
]
// 