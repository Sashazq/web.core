	document.execCommand('styleWithCSS',false,true)
	
	function saveselection(e){
		if (document.getSelection) {
      var sel = document.getSelection();
    	if(sel.rangeCount > 0) core.range = sel.getRangeAt(0);
    }
    $('img.img').off('click', function(e){
			if($('#adminpanel').hasClass('open')){
				$('.selected').removeClass('selected')
				$(this).toggleClass('selected')
				console.dir($(this))
			}
		})
		$('img.img').on('click', function(e){
			if($('#adminpanel').hasClass('open')){
				$('.selected').removeClass('selected')
				$(this).toggleClass('selected')
				console.dir($(this))
			}
		})
	}
	$('#savepage').click(function(){
		page = {
			name:window.location.pathname,
			content:'<div id="content">'+$('#content').html()+'</div>'
		}
		core.ws.emit('save',page)
	})
	$('#floatLeft').click(function(e){
		$('.img.selected').css('float', 'left')
	})
	$('#floatRight').click(function(e){
		$('.img.selected').css('float', 'right')
	})

	$('#fontface').change(function(e){
		console.dir($(this))
		if (core.range != undefined && document.getSelection ) {
      var sel = document.getSelection();
      if(sel.rangeCount > 0){
      	sel.removeAllRanges();
      	sel.addRange(core.range);
      }
    }
		document.execCommand('fontName',false, e.currentTarget.value);
	})
	$('.colorsample').click(function(e){
		$(this).parent().addClass('hidden');
		if (core.range != undefined && document.getSelection ) {
      var sel = document.getSelection();
      if(sel.rangeCount > 0){
      	sel.removeAllRanges();
      	sel.addRange(core.range);
      }
    }
		document.execCommand($(this).parent().parent().attr('id'),false, $(this).css('background-color'));
	})
	$('#backColor').mouseover(function(e){
		$(this).find('.colorpicker').removeClass('hidden')
	})
	$('#backColor').mouseout(function(e){
		$(this).find('.colorpicker').addClass('hidden')
	})
	$('#foreColor').mouseover(function(e){
		$(this).find('.colorpicker').removeClass('hidden')
	})
	$('#foreColor').mouseout(function(e){
		$(this).find('.colorpicker').addClass('hidden')
	})
	$('#createlink').click(function(e){
		$(this).find('.linkform').toggleClass('hidden')
	})
	$('.linkform').click(function(e){
		e.stopPropagation()
	})
	$('#linkapply').click(function(e){
		if (core.range != undefined && document.getSelection ) {
      var sel = document.getSelection();
      if(sel.rangeCount > 0){
      	sel.removeAllRanges();
      	sel.addRange(core.range);
      }
    }
		$(this).parent().addClass('hidden')
		document.execCommand('createLink',false, $(this).parent().find('#linkurl').val());
	})
	$('#fontsize').change(function(e){
		console.dir($(this))
		if (core.range != undefined && document.getSelection ) {
      var sel = document.getSelection();
      if(sel.rangeCount > 0){
      	sel.removeAllRanges();
      	sel.addRange(core.range);
      }
    }
		document.execCommand('fontSize',false, (e.currentTarget.value - 6)>>1);
	})
	$('.toolbutton').mouseout(function(e){
		$(this).removeClass('hover')
		var bgpos = $(this).css('background-position').split("px").join('').split(' ')
		$(this).css('background-position',bgpos[0]+'px '+(bgpos[1]*1-240)+'px');
	});
	$('.toolbutton').mouseover(function(e){
		$(this).addClass('hover')
		var bgpos = $(this).css('background-position').split("px").join('').split(' ')
		$(this).css('background-position',bgpos[0]+'px '+(bgpos[1]*1+240)+'px');
//		if (!$(this).hasClass('no-save-selection') && document.getSelection) {
//      var sel = document.getSelection();
//    	if(sel.rangeCount > 0)core.range = sel.getRangeAt(0);
//    	console.dir('toolbutton')
//    }
	});
	$('.editbutton').click(function(e){
		if (core.range != undefined && document.getSelection ) {
      var sel = document.getSelection();
      if(sel.rangeCount > 0){
      	sel.removeAllRanges();
      	sel.addRange(core.range);
      }
    }
		document.execCommand($(this).attr('id'),$(this).attr('data-ui') || false, $(this).attr('data-value') || false);
	})
	$('#logout').click(function(e){
		core.ws.emit('logout',{login: ::template.login::})
	})
	$('#escapechanges').click(function(e){
		window.location = window.location.pathname;
	});
	$('#dockpanel').toggle(
		function(){
			console.log('toggle')
			$('#content').attr('contenteditable',true);
			$('#adminpanel').removeClass('closed').addClass('open');
			$('img.img').on('click', function(e){
				if($('#adminpanel').hasClass('open')){
					$('.selected').removeClass('selected')
					$(this).toggleClass('selected')
					console.dir($(this))
				}
			})
			if(core.contentinited == undefined){
				$('#content[contenteditable="true"]').find('p,h1,h2,h3,h4,h5,h6,hr,span,b,i').keyup(saveselection).mouseup(saveselection);
				core.contentinited = true;
			}
		},
		function(){
			console.log('toggle')
			$('#content').removeAttr('contenteditable');
			$('.selected').removeClass('selected')
			$('#adminpanel').addClass('closed').removeClass('open');
			$('img.img').off('click', function(e){
				if($('#adminpanel').hasClass('open')){
					$('.selected').removeClass('selected')
					$(this).toggleClass('selected')
					console.dir($(this))
				}
			})
		}
	);
	