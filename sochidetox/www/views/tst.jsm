var index = exports.index = {};
index.title = 'Доктор Эленшлегер';
index.phone = '(495) 789-4444';
index.address = 'Москва, проспект Мира, д. 90';
index.page = function(page){
	var jsm = 
	{html:{ html:[ 
		{head:{ html:[
			{title:{ a:"b", html:[ index.title ] }},
			{link:{ rel:"icon", type:"image/vnd.microsoft.icon", href:"/img/fav.ico" }},
			{link:{ type:"text/css", href:"/css/jquery-ui-1.8.22.custom.css", rel:"stylesheet" }},
			{link:{ type:"text/css", href:"/css/style.css", rel:"stylesheet" }},
			{script:{ type:"text/javascript", src:"/js/jquery-1.7.2.min.js" }},
			{script:{ type:"text/javascript", src:"/js/jquery-ui-1.8.22.custom.min.js" }},
			{script:{ type:"text/javascript", src:"/js/script.js" }}
		] }},
		{body:{ html:[
			{div:{ id:'page', html:[
				{a:{ href:'/', html:[ {div:{ id:'header', class:"abs full-width" }} ] }},
				(function(menu, items){
					menu.html = menu.html || [];
					for(id in items){
						if(items[id].href){
							menu.html.push( {div:{ id:id, class:'menuitem', html:[ {a:{ class:'caption', href:items[id].href, html:[ items[id].caption ] }} ] }} )
						} else if(items[id].sub) {
							var sub = {div:{ class:'submenu', html:[] }};
							for(idx = 0; idx < items[id].sub.length; idx++){
								if(typeof items[id].sub[idx] == "string") {
									sub.html.push( {div:{ class:'category', html:[ items[id].sub[idx] ] }} );
								} else if(typeof items[id].sub[idx] == "object") {
									sub.html.push({a:{ href:items[id].sub[idx].href, html:[ {div:{class:'subitem', html:[ items[id].sub[idx].caption ] }} ] }});
								}
							}
							menu.html.push({div:{ id:id, class:'menuitem', html:[ {div:{ class:'caption', html:[items[id].caption, sub ] }} ] }})
						}
					} 
					return menu;
				})(
					{div:{ id:'menu', class:"abs full-width" }},
					{
						doctor:{
							caption:"ДОКТОР ЭЛЕНШЛЕГЕР",
							sub:[
								{ caption: "БИОГРАФИЯ", href:"/biography" },
								{ caption: "ИНСТИТУТ ПЛАСТИЧЕСКОЙ ХИРУРГИИ", href:"/puc" }
							]
						},
						operations:{
							caption:"ВИДЫ ОПЕРАЦИЙ",
							sub:[
								"ЛИЦО",
								{ caption: "БЛЕФАРОПЛАСТИКА", href:"/puc" },
								{ caption: "ФЕЙСЛИФТИНГ", href:"/puc" },
								{ caption: "ЛИФТИНГ ЛБА", href:"/puc" },
								{ caption: "РИНОПЛАСТИКА", href:"/puc" },
								{ caption: "ОТОПЛАСТИКА", href:"/puc" },
								{ caption: "ИМПЛАНТЫ НИЖНЕЙ ЧЕЛЮСТИ", href:"/puc" },
								"ТЕЛО",
								{ caption: "ЛИПОКСАЦИЯ", href:"/puc" },
								{ caption: "АБДОМИНОПЛАСТИКА", href:"/puc" },
								{ caption: "ЭНДОПРОТЕЗИРОВАНИЕ ГОЛЕНЕЙ", href:"/puc" },
								{ caption: "УДАЛЕНИЕ РУБЦОВ", href:"/puc" },
								"ГРУДЬ",
								{ caption: "УВЕЛИЧЕНИЕ ГРУДИ", href:"/puc" },
								{ caption: "ВИДЫ ИМПЛАНТОВ", href:"/puc" },
								{ caption: "УМЕНЬШЕНИЕ ГРУДИ", href:"/puc" },
								{ caption: "ПОДТЯЖКА ГРУДИ", href:"/puc" },
								{ caption: "ИЗМЕНЕНИЕ ФОРМЫ ГРУДИ", href:"/puc" },
								{ caption: "ИЗМЕНЕНИЕ ФОРМЫ СОСКОВ И АРЕОЛ", href:"/puc" }
							]
						},
						contacts:{ caption:"КОНТАКТЫ",href:"/puc" },
						questions:{
							caption:"ВОПРОСЫ И ОТВЕТЫ",
							sub:[
								{ caption: "ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ", href:"/puc" },
								{ caption: "ЗАДАЙТЕ ВАШ ВОПРОС", href:"/puc" },
							]
						},
						comments:{ caption:"ОТЗЫВЫ", href:"/puc" }
					}
				),
				{div:{ id:'body', html:[
					
					require(__dirname+'/'+page+'.jsm').content,				//TODO: from mongo	

					{div:{ class:'clear' }}
				] }},
				{div:{ id:'footer', class:"abs full-width", html:[
					{div:{ id:'foot', html:[
						{div:{ id:'footmenu', html:[
							{a:{ href:'/tpl', html:[ 'доктор Эленщлегер' ] }},' | ',{a:{ href:'/tpl', html:[ 'контакты' ] }},' | ',{a:{ href:'/tpl', html:[ 'карта сайта' ] }}
						] }},
						{div:{ id:'contactinfo', html:[
							{a:{ href:'http://goo.gl/maps/1rBeS', target:"_blank", html:[ 'Метро «Цветной бульвар», Малый Сухаревский пер., д. 10.' ] }},
							'<br/>Часы работы: с 9:00 до 22:00 ежедневно, без перерыва на обед.<br/>',
							'<b>Звоните круглосуточно тел.(495) 789-4444<b/>'
						] }}
					] }}
				] }}
			] }},
			{div:{ id:'orderholder', class:"full-width", html:[
				{div:{ id:'orderplace', html:[
					{div:{ id:'orderwindow', html:[
						{div:{ id:'orderheader', class:"full-width" }},
						{div:{ id:'orderbody', class:"abs full-width", html:[
							{form:{ id:"order", name:"orderform", action:"/order", method:"post", html:[
								'Напишите, какая у Вас проблема, и наши специалисты свяжутся с Вами в течение 5 минут.',
								'<br/>Ваше имя:<br/>',
								{input:{ name:"name", type:"text", form:"order" }},
								'<br/>Контактный телефон:<br/>',
								{input:{ name:"phone", type:"text", form:"order" }},
								'<br/>Ваша проблема:<br/>',
								{textarea:{ name:"message", form:"order" }},
								'<br/>',
								{input:{ type:"submit", value:"Отправить", form:"order" }}
							] }}
						] }}
					] }}
				] }}
			] }}
		] }}
	] }};
	delete require.cache[__dirname+'/'+page+'.jsm'];
	return jsm;
}
