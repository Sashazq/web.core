module.exports.script = ['script',{src:'/js/fotorama.js'}];
module.exports.style = ['link',{ type:"text/css", href:'/css/fotorama.css', rel:"stylesheet" }];
var fs = module.parent.exports.fs;
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		(function(){
			var frama = ['div', {
				class:'fotorama', style:'margin-top:10px;',
				'data-width':"705", 'data-height':"425",
				'data-loop':'true', 'data-minPadding':'0',
				'data-minWidth':'705', 'data-minHeight':'425',
				'data-thumbSize':'64', 'data-navBackground':'#490000',
				'data-background':'#490000', 'data-thumbBorderColor':'#ff0000'
			}],
			cnt = fs.readdirSync(__dirname+'/../../public/img/galery/wellness/').length;
			for(i=1;i<=cnt;i++) frama.push(['img',{src:'/img/galery/wellness/'+i+'.png'}])
			return frama;
		})(),
	]
]
// style="text-decoration:underline;"