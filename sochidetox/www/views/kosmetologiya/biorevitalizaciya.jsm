module.exports.content = 
['div',{id:'content'},
	['div', {class:"text"},
		['div', {class:"frameimg", style:"width:634px;margin-left:auto;margin-right:auto;"},
			['img', {style:'margin-bottom:-4px;', src:"/img/pages/frame/1.png"}],['img', {style:'float:left;', src:"/img/pages/frame/2.png"}],
			['img', {style:'float:left;', src:"/img/pages/cosmetology/biorevitalization/1.png"}],
			['img', {src:"/img/pages/frame/3.png"}],['img', {style:'margin-top:-4px;', src:"/img/pages/frame/4.png"}]
		],
		'<h1>Биоревитализация</h1><hr/>',
		'<p>Одна из главных причин ухудшения внешнего вида кожи – это потеря влаги. Этому способствует и агрессивная окружающая среда (солнце, холод, ветер), и внутренние факторы: со временем (начиная примерно с 25 лет) в дерме неуклонно снижается выработка гиалуроновой кислоты. Именно это вещество отвечает за удержание в коже влаги. С годами кислоты в тканях становится все меньше, а значит, кожа уже не может выглядеть как хорошо, как раньше. Снижается тургор, ухудшается цвет, появляются морщины. «Напоить» кожу, обеспечив ей запас гиалуроновой кислоты, можно с помощью процедуры биоревитализации.</p>',
		'<p>В ходе <b>процедуры биоревитализации</b> гиалуроновая кислота вводится непосредственно под кожу. Попав в дерму, она начинает активно работать и стимулирует выработку организмом собственной гиалуроновой кислоты. Разглаживаются морщины, усиливается микроциркуляция, к коже возвращается увлажненность и эластичность. </p>',
		'<p>Современная косметология предлагает множество препаратов для биоревитализации. Косметолог подберет Вам средство исходя из Ваших индивидуальных особенностей. </p>',
		'<p>В Mishilen Detox & Wellness мы используем только лучшие препараты для биоревитализации, доказавшие свою эффективность.</p>',
		'<p><i >Препарат Ial-system</i> считается «золотым стандартом» биоревитализации, он очень надежен, безопасен и эффективен. Это эксклюзивная разработка итальянской компании PHITOGEN, широко известной в Европе как производитель высококачественных косметологических препаратов для профессионального использования. </p>',
		'<p>Еще один известный итальянский производитель, продукцию которого используют наши косметологи -  компания IBSA. Эта компания считается крупнейшим производителем гиалуроновой кислоты в мире. <i >Препарат Viskoderm</i> от IBSA  имеет высокую степень очистки и способен адаптироваться к биологическому возрасту кожи. Это позволяет достигать отличных результатов – красивая молодая кожа на долгие годы! </p>',
		'<p><b>Кому необходима процедура биоревитализации?</b></p>',
		'<li>Тем, кто старше 25 лет;</li>',
		'<li>Тем, кто регулярно посещает солярий или любит позагорать летом на пляже;</li>',
		'<li>Тем, кто перенес долгую болезнь, отразившуюся на состоянии кожи;</li>',
		'<li>Тем, кто быстро и резко сбросил вес;</li>',
		'<li>Тем, кто проходил процедуры, связанные с агрессивным воздействие на кожу, например, глубокие пилинги;</li>',
		'<li>Всем, кто хочет улучшить свой внешний вид, повысить тонус и упругость кожи, избавится от морщин и следов усталости.</li>',
		'<p>Биоревитализация является также <b>прекрасной профилактикой против преждевременного старения</b>, поэтому ее можно использовать даже тогда, когда нет явных показаний – так Вы сможете сохранить цветущий внешний вид на долгое время!</p>'
	]
]
// 