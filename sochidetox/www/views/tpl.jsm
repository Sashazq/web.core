var sys = require('util')
	, fs = module.exports.fs = require('fs')
	, puc = module.exports.puc = function(page){ return fs.existsSync(__dirname+page+'.jsm')?page:"/puc" }
	, Template = function(){};
Template.prototype = {
	
	getPage:function(data){ 
		return this.pagedata[data] || this.page[data] || false; 
	},
	
	title:'Mishilen',
	phone:'(495) 788-34-94',
	address:'Краснодарский край, г. Сочи,<br/>ул. Дорога на Большой Ахун, дом 7/1<br/>',
	
	menu:['div',{id:'menu', class:"abs full-width noprint"}],
	menuItem:function(menuItems, id){
		var m = 
		['div', { id:id, class:'menuitem hoverable normal' },
			['div',{class:'abs onhover skin normal'}],
			['a', { class:'onhover normal', href:menuItems[id].href }, ['div',{},menuItems[id].caption]]
		]
		return m
	},
	menuItems:(function(tpl){
		return {
			about:{ caption:"О НАС", href:puc("/o-nas") },
			price:{ caption:"ЦЕНЫ", href:puc("/ceny") },
			specpropositions:{ caption:"СПЕЦПРЕДЛОЖЕНИЯ", href:puc("/specpredlozheniya") },
			contacts:{ caption:"КОНТАКТЫ", href:puc("/contacty") },
			questions:{ caption:"ВОПРОСЫ И ОТВЕТЫ", href:puc("/voprosy-i-otvety") },
			news:{ caption:"НОВОСТИ", href:puc("/novosti") }
		}
	})(this),
	
	sideMenu:['div',{id:'sideMenu', class:"full-width noprint"}],
	sideMenuItem:function(menuItems, id, sub){
		var m = 
		['a',{href:menuItems[id].href},
			['div', { id:id, class:'sidemenuitem hoverable normal' },
				['div',{class:'onhover skin normal'},
					['div',{class:"full-width "+menuItems[id].skin, style:"height:100%;"}],
					['div',{class:'abs caption'},menuItems[id].caption],
					sub || ''
				],
			],
		]
		return m
	},
	sideMenuItems:(function(){
		return {
//			reserve:{ skin:"reserve", caption:"ЗАБРОНИРОВАТЬ НОМЕР", href:puc("/reserve") },
			cleandetox:{ skin:"sideitem", caption:"ОЧИЩЕНИЕ<BR/>MISHILEN DETOX & WELLNESS", href:puc("/cleandetox"),
				sub:[
					{ caption: "ЗАЧЕМ НУЖНО ОЧИЩЕНИЕ НАМ", href:puc("/mishilen/zachem-nuzhno-ochishchenie") },
					{ caption: "ДЕТОКС-ПРОГРАММЫ", href:puc("/mishilen/detox-programmy") },
					{ caption: "МЕДИЦИНСКОЕ ОБСЛУЖИВАНИЕ", href:puc("/mishilen/medicinskoe-obsluzhivanie") }
				] 
			},
			wellness:{ skin:"sideitem", caption:"WELLNESS-ЦЕНТР", href:puc("/wellness"),
				sub:[
					{ caption: "АКВАТЕРАПИЯ", href:puc("/wellness-centr/aquaterapiya") },
					{ caption: "БАНИ И САУНЫ", href:puc("/wellness-centr/bani-i-sauny") },
					{ caption: "МАССАЖИ", href:puc("/wellness-centr/massazhi") } ,
					{ caption: "ПРЕССОТЕРАПИЯ", href:puc("/wellness-centr/pressoterapiya") },
					{ caption: "ОБЁРТЫВАНИЯ И ПИЛИНГИ", href:puc("/wellness-centr/obertyvaniya-pilingi") },
					{ caption: "СПА-КАПСУЛА", href:puc("/wellness-centr/spa-capsula") },
					{ caption: "ПРОФЕССИОНАЛЬНАЯ КОСМЕТИКА<BR/>ОТ ALGOLOGIE И STYX", height:'46px', href:puc("/wellness-centr/professionalnaya-kosmetika-algologie-i-styx") },
					{ caption: "КАВИТАЦИЯ", href:puc("/wellness-centr/cavitfciya") },
					{ caption: "ИНЪЕКЦИОННЫЕ МЕТОДИКИ", href:puc("/wellness-centr/injectionnye-metodiki") },
					{ caption: "ИГЛОРЕФЛЕКСОТЕРАПИЯ", href:puc("/wellness-centr/iglorefleksoterapiya") },
					{ caption: "БАССЕЙН И АКВААЭРОБИКА", href:puc("/wellness-centr/bassein-i-aquaaerobika") },
					{ caption: "ЙОГА И ПИЛАТЕС", href:puc("/wellness-centr/yoga-i-pilates") },
				]
			},
			cosmetology:{ skin:"sideitem", caption:"КОСМЕТОЛОГИЯ", href:puc("/cosmetology"),
				sub:[
					{ caption: "УХОДЫ, МАССАЖИ, МАСКИ, ПИЛИНГИ", href:puc("/kosmetologiya/uhody-massazhi-maski-pilingi") },
					{ caption: "ФИЛЛЕРЫ, БОТОКС", href:puc("/kosmetologiya/fillery-botox") },
					{ caption: "БИОРЕВИТАЛИЗАЦИЯ", href:puc("/kosmetologiya/biorevitalizaciya") },
					{ caption: "СПА-ПРОЦЕДУРЫ ДЛЯ ГРУДИ,<br/>РУК И НОГ", href:puc("/kosmetologiya/spa-procedury-dlya-grudi-ruk-i-nog") }
				]
			},
			residence:{ skin:"sideitem", caption:"ПРОЖИВАНИЕ", href:puc("/residence"),
				sub:[
					{ caption: "БУТИК-ОТЕЛЬ <br/>«MISHILEN DETOX & WELLNESS»", href:puc("/prozhivanie/butic-otel-mishlen-detox-and-wellness") },
					{ caption: "НОМЕРА", href:puc("/prozhivanie/nomera") },
					{ caption: "РЕСТОРАН", href:puc("/prozhivanie/restoran") }
				]
			}
		}
	})(),
	
	createMenu:function(menu, menuItems, menuItem){
		for(id in menuItems){
			var frst = true;
			var sub = ['div', { class:'abs submenu'}]
			console.dir(['>>', menuItems[id]]);
			if(typeof menuItems[id] == 'string'){
				menu.push( ['div', { class:'category' }, menuItems[id]] )
			} else if ( typeof menuItems[id] == 'object' ){
				if(menuItems[id].href) menu.push(menuItem(menuItems, id, menuItems[id].sub ? sub : null));
				if(menuItems[id].sub) {
						var subitem = function(items, id){
								var m=
								['div', { id:id, class:'subitem hoverable normal', style:items[id].height ? "height:"+items[id].height : "" },
									['a', { class:'abs onhover normal', href:items[id].href }, ['div',{class:'tst'},items[id].caption] ],
									(function(){if(frst) {frst = false; return '';} else return '<hr class="subhr">' })()
								]
								return m;
							};				
					this.createMenu(sub, menuItems[id].sub, subitem)
				}
			}
		} 
		return menu;
	},
	
	loggedin:false,
	adminPanel:function(){
		if(!this.loggedin){
			panel =
			['div', { id:"sidepanel", class:"abs closed noprint"}, 
				['div', { id:"login", class:"panel_closed"}], 
				['div', { id:"loginpanel"}, 
					['input', { type:"text", id:"user"}],
					['input', { type:"password", id:"passw"}]
				]
			]
			//console.log(sys.inspect(panel,true,5));
			return panel;
		} else {
			return ['div', {id:"admin", class:'abs panel_closed'}]
		}
	},
	
	get:function(page, pagedata){
		this.page = require(__dirname+'/'+page+'.jsm');
		this.page.name = page;
		this.pagedata = pagedata || {name:'/'+page};
		var jsm = ['html', {}, 
			['head', {},
				['title', {}, this.title ],
				['meta',{charset:"UTF-8",'http-equiv':"Content-Type", content:"text/html; charset=utf-8"}],
				['meta',{name:"viewport", content:"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"}],
				['meta',{name:"apple-mobile-web-app-capable", content:"yes"}],
				['link', { rel:"icon", type:"image/vnd.microsoft.icon", href:"/img/fav.ico" }],
				//['link', { type:"text/css", href:"/css/stylesheet.css", rel:"stylesheet" }],
				['link', { type:"text/css", href:"/css/jquery-ui-1.9.0.custom.min.css", rel:"stylesheet" }],
				['link', { type:"text/css", href:"/css/style.css", rel:"stylesheet" }],
				this.getPage('style'),
				['script', { type:"text/javascript", src:"http://"+this.vhost.host+this.vhost.zone+":"+this.vhost.sioport+"/socket.io/socket.io.js" }],
				['script', { type:"text/javascript", src:"/js/jquery-1.8.2.min.js" }],
				['script', { type:"text/javascript", src:"/js/jquery-ui-1.9.0.custom.min.js" }],
				['script', { type:"text/javascript"},
					"window.location.sioport = '", this.vhost.sioport.toString(),"';",
					"window.location.sions = '", this.vhost.host,"';",
					"window.pageID = 'page_", this.page.name,"';",
					"var console = console || {log:function(){}, dir:function(){}};"
				],
				['script', { type:"text/javascript", src:"/js/script.js" }],
				this.getPage('script')
			],
			['body', {},
				['div',{class:'page', id:'page_'+page},
					['a',{href:'/'},['div',{id:'header', class:"abs full-width noprint"},['div',{class:'abs phonepng'}],['div',{class:'abs phonenum'},this.phone]]],
					this.createMenu(this.menu, this.menuItems, this.menuItem),
					['div', {class:"full-width line"}],
					['div', {class:"full-width back"},['div', {class:"full-width longshadow"}]],
					['div',{id:'body'},
						['div',{id:'SideBar', class:"noprint"},
							['a',{href:puc('/zabronirovat-nomer')},
								['div', { id:'reserve', class:'sidemenuitem hoverable normal' },
									['div',{class:'onhover skin normal'},
										['div',{class:"full-width reserve", style:"height:100%;"}],
										['div',{class:'abs caption'}, 'ЗАБРОНИРОВАТЬ НОМЕР']
									]
								]
							],
							(function(smi){
								var sm = ['div',{class:'sideMenu'}], tab = 0;
								for(hdr in smi){
									var sub = ['div',{}];
									for(idx=0; idx < smi[hdr].sub.length; idx++){
										sub.push(['a',{'data-tab':''+tab, href:smi[hdr].sub[idx].href},smi[hdr].sub[idx].caption])
									}
									tab++;
									sm.push(['h3',{id:hdr, class:'sidemenuitem hoverable normal'+smi[hdr].skin},smi[hdr].caption],sub)
								}
								return sm;
							})(this.sideMenuItems),
							//(page!='index' ? ['div', {class:"clear full-width back", style:"top:-1px;height:22px;"},['div', {class:"full-width shortshadow"}]]:''),
							['div', {class:"clear full-width back", style:"top:-1px;height:22px;"},['div', {class:"full-width shortshadow"}]],
							['div',{class:'imgholder'}/*,['img', {alt:"", src:(this.getPage('sideimage') || '/img/girl.png')}]*/]
						],
						this.getPage('content'),
						['div',{class:'clear'}]
						//(page=='index' ? ['div', {class:"clear full-width back", style:"top:-14px;height:0px;"},['div', {class:"full-width longshadow"}]]:'')
					],
					['div', {class:"full-width fotos"},
						['div', {class:"clear"}],
						['a',{class:'afoto',href:'/galereya/mishelen-detox'},['div', {class:'foto1'}]],
						['a',{class:'afoto',href:'/galereya/nomera'},['div', {class:'foto2'}]],
						['a',{class:'afoto',href:'/galereya/wellness-centr'},['div', {class:'foto3'}]]
					],
					['div', {class:"clear full-width back", style:"top:0px;height:22px;"},['div', {class:"full-width longshadow"}]], 
					['div', {class:'full-width footer'},
						['div', {class:'footmenu'},
							['a',{href:'/sitemap'},'карта сайта'],'&nbsp&nbsp&nbsp&nbsp&nbsp',
							['a',{href:'/contacty'},'контакты'],
							['div',{class:'clear'}],
							['div',{class:'liveinternet', style:"margin-top:35px;"}]
						],
						['div', {class:'footinfo'}, this.address,' Телефон в Москве: ', this.phone]
					],
					['div', {class:"clear full-width back", style:"top:0px;height:22px;"},['div', {class:"full-width longshadow"}]]
				],
				
				this.adminPanel()
			]
		];
		delete require.cache[__dirname+'/'+page+'.jsm'];
		return jsm;
	}
}

module.exports.template = new Template();
