module.exports.script = ['script', {type:'text/javascript', src:'/js/reserve.js'}]
module.exports.content =
['div',{id:'content'},
	['div', {class:"text"},
		'<h1 style="line-height: 50%; padding-top: 15px; padding-bottom: 20px;">Для успешного бронирования заполните, пожалуйста, поля, расположенные ниже:</h1>',
		'<hr style="margin-right: 25px;"/>',
		['div',{class:'formitem'},['div',{class:'reformname'},'Имя'],['input',{class:'reform req', type:'text', id:'fname'}]],
		['div',{class:'formitem'},['div',{class:'reformname'},'Фамилия'],['input',{class:'reform req',type:'text', id:'lname'}]],
		['div',{class:'formitem'},['div',{class:'reformname'},'Дата заезда'],['input',{class:'reform req',type:'text', id:'data'}]],
		['div',{class:'formitem'},['div',{class:'reformname'},'Количество дней'],['select',{class:'reform req',type:'text', id:'days'},
			['option',{},'3'],['option',{},'7'],['option',{},'10'],['option',{},'14']]
		],
		['div',{class:'formitem'},['div',{class:'reformname'},'Количество человек'],['input',{class:'reform req',type:'text', id:'persons'}]],
		['div',{class:'formitem'},['div',{class:'reformname'},'Количество номеров'],['input',{class:'reform req',type:'text', id:'apparts'}]],
		'<br><hr style="margin-top:10px;margin-right: 25px;"/>',
		['div',{class:'formitem'},['div',{class:'reformname'},'Телефон'],['input',{class:'reform',type:'text', id:'phone'}]],
		['div',{class:'formitem'},['div',{class:'reformname'},'e-mail'],['input',{class:'reform',type:'text', id:'mail'}]],
		['div',{class:'formitem'},['div',{class:'reformname'},'Дополнительная информация'],['textarea',{class:'reform',type:'text', id:'description'}]],
		['div',{class:'formsend'},'ОТПРАВИТЬ']
	]
]
// style="text-decoration:underline;"