var page = {
	init:function(core){
		if(!core || !core.ws) return;
		this.core = core;
		self = this;
		$('.formitem #data').datepicker({
			closeText:'X', dateFormat:'dd.mm.yy', firstDay:1,
			dayNames:['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
			dayNames:['Вос','Пон','Втр','Срд','Чет','Пят','Суб'],
			dayNamesMin:['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
			monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			monthNamesShort:['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
			nextText:'След',prevText:'Пред', showOptions:{direction: 'down'}, weekHeader:'Нд'
		})
		$('.formsend').on({
			'mousedown':function(e){
				$(this).addClass('down');
			},
			'mouseup':function(e){
				$(this).removeClass('down');
			},
			'click':function(e){
				var reservedata = {}, valid = true;
				$('.reform').each(function(idx){
					if($(this).hasClass('req') && $(this).val().length == 0) valid = false;
					reservedata[$(this).attr('id')] = $(this).val();
				})
				if(valid) self.reserve(reservedata); else alert('все общие данные обязательны к заполнению')
			}
		})
	},
	reserve: function (data){
		this.core.ws.emit('reserved', { data: data });
		$('#content .text').html(
			'<div class="formthanks full-width">'+
			'<p style="font:20px Arial;">Благодарим Вас за то, что Вы выбрали MishilenDetox & Wellness!</p>'+
			'<p>Информация, которую Вы предоставили, уже передана</p>'+
			'<p>в отдел бронирования. В ближайшее время наш менеджер</p>'+
			'<p>свяжется с Вами для подтверждения заказа.</p>'+
			'</div>'
		)
		//console.log(data)
	}
}