var page = {
	init:function(core){
		if(!core || !core.ws) return;
		this.core = core;
		core.ws.on('getgalery', function(data){
			core.ws.emit('init', {})
			$('.galeryholder').html(data.section);
			$('.galerycontent').accordion({
				autoHeight: false,
				navigation: true,
				collapsible:true/*,
				event: "mouseover"*/
			}).bind('accordionchange', function(event, ui) {
				var prev = $(ui.newHeader).prev().prev()
				if(prev.length) {
			  	$("html, body").animate({scrollTop: prev.offset().top+30}, 500, 'easeOutExpo');
				}
			})
		});
		this.getGalery('all');
	},
	getGalery: function (section){	
		this.core.ws.emit('getgalery', { section: section });
		console.log('get galery data')
	}
}