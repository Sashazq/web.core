var undefined, console = console || {log:function(){},dir:function(){}}, core = {
	ws: io.connect('wss://'+window.location.host+':'+window.location.sioport+'/'+window.location.sions),
	emit:function(evt, data){
		this.ws.emit(evt,data)
	},
	init: function (){

		var core = this
			, cat = window.location.pathname.split('/')[1]
			, cp = $('a[href="'+window.location.pathname+'"]')
			, categories = {
				'mishilen':0,
				'wellness-centr':1,
				'kosmetologiya':2,
				'prozhivanie':3
			}
			, category = categories[cat] == undefined ? 4 : categories[cat];
		cp.addClass('currentpage');
		//console.log(cat);
		//console.log(category);
		//cp.prev().removeClass('normal').addClass('hovered');
		
		core.ws.on('run',function(code){eval(code)})
		
		core.ws.on('init',function(initial){
			//console.dir(['on init', initial])
		})
		
		core.ws.on('login',function(profile){
			if(profile.auth){
				$('#sidepanel').replaceWith(profile.panel);
				//console.log(profile.init)
				eval(profile.init);
			} else {
				$('#sidepanel').parent().removeClass('closed').addClass('open');
				alert("Не верное имя пользователя или ошибка в пароле");
			}
		})
		core.ws.on('logout',function(profile){
			//console.log('logout')
			window.location = window.location.pathname;
//			if(profile.auth){
//				$('#sidepanel').replaceWith(profile.panel);
//				console.log(profile.init)
//				eval(profile.init);
//			} else {
//				$('#sidepanel').parent().removeClass('closed').addClass('open');
//				alert("Не верное имя пользователя или ошибка в пароле");
//			}
		})
		$('input#passw').keypress(function(event) {
  		if ( event.which == 13 ) {
  			var login = $('input#user').val();
				var passw = $('input#passw').val();
  			if(login.length > 2 && passw.length > 5){
					core.ws.emit('login',{login:login,passw:passw})
				}
  		}
		});
		$('#login').click(function(e){
			if($(this).parent().hasClass('open')){
				var login = $('input#user').val();
				var passw = $('input#passw').val();
				if(login.length > 2 && passw.length > 5){
					core.ws.emit('login',{login:login,passw:passw})
				}
				$(this).parent().removeClass('open').addClass('closed');
			} else {
				$(this).parent().removeClass('closed').addClass('open');
			}
		});
		$('.hoverable').hover(
			function(e){
				$(this).find('.onhover').removeClass('normal').addClass('hovered')
				var cp = $('a[href="'+window.location.pathname+'"]');
				cp.removeClass('normal').addClass('hovered');
				cp.prev().removeClass('normal').addClass('hovered');
			} , 
			function(e){
				$(this).find('.onhover').addClass('normal').removeClass('hovered')
				var cp = $('a[href="'+window.location.pathname+'"]');
				cp.removeClass('normal').addClass('hovered');
				cp.removeClass('normal').prev().addClass('hovered');
			} 
		);
		$('#sendorder').click(function(e){
			$('#orderholder').removeClass('hidden');
		});
		$('#closeorder').click(function(e){
			$('#orderholder').addClass('hidden');
		});
		$('#order #submit').click(function(e){
			$('#orderholder').addClass('hidden');
			core.ws.emit('order',{name:$('#name').val(), phone:$('#phone').val(), message:$('#message').val()})
		});
		$('#page_faq #submit').click(function(e){
			core.ws.emit('question',{q:$('#question').val(), email:$('#email').val()})
			$('#question').val('');
			$('#email').val('')
		});

		$('.sideMenu').accordion({
				active:category,
				animate:{duration:300, easing:'easeInOutCubic'},
				icons:{header: false, activeHeader: false },
				heightStyle:'content',
				collapsible:true
		});
		if(
			window.page != undefined && 
			window.page.init != undefined && 
			typeof window.page.init == 'function'
		) window.page.init(this);
		core.ws.emit('init',true);
		liveinternetsrc='//counter.yadro.ru/hit?t14.1;r'+escape(document.referrer)+((typeof(screen)=='undefined')?'':';s'+screen.width+'*'+screen.height+'*'+(screen.colorDepth?screen.colorDepth:screen.pixelDepth))+';u'+escape(document.URL)+';'+Math.random();
		liveinterneturl='http://www.liveinternet.ru/click'
		$('.liveinternet').html('<a href='+liveinterneturl+' target=\'_blank\'><img src='+liveinternetsrc+' alt=\'\' title=\'LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня\'  border=\'0\' width=\'88\' height=\'31\'></a>')
	}
}
$(function(){	core.init() })