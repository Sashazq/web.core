var page = {
	init:function(core){
		if(!core || !core.ws) return;
		this.core = core;
		self = this;
		$('.formsend').on({
			'mousedown':function(e){
				$(this).addClass('down');
			},
			'mouseup':function(e){
				$(this).removeClass('down');
			},
			'click':function(e){
				var questiondata = {}, valid = true;
				$('.reform').each(function(idx){
					if($(this).hasClass('req') && $(this).val().length == 0) valid = false;
					questiondata[$(this).attr('id')] = $(this).val();
				})
				if(valid) self.question(questiondata); else alert('все данные обязательны к заполнению')
			}
		})
	},
	question: function (data){
		this.core.ws.emit('question', { data: data });
		$('#content .text').html(
			'<div class="formthanks full-width">'+
			'<p style="font:20px Arial;">Благодарим Вас за то, что Вы проявили интерес к MishilenDetox & Wellness!</p><br/>'+
			'<p>Ваш вопрос принят и в ближайшее время мы Вам на него ответим.</p>'+
			'</div>'
		)
		//console.log(data)
	}
}