var undefined
	, VirtualHost = function(config){
			this.config = config;
			this.events = this.events || {}
			for(setting in config) this[setting] = config[setting];
			this.upload = this.upload || __dirname + "/public/uploads";
			this.static = this.static || __dirname + '/public';
			this.secret = this.secret || "rkfdbfnehysq rjn";
		}
	, ViHost = {
			  sys: require('util')
			, fs : require('fs')
			, on:function(evt, callback){
					this.events[evt] = this.events[evt] || []
					this.events[evt].push(callback)
				}
			, start:function(core){
					var vh = this
					for(setting in core) this[setting] = this[setting] || core[setting];
					this.app = this.express.createServer();
					this.app.configure(function(){
					  vh.app.use(vh.express.bodyParser({ uploadDir: __dirname + "/public/uploads" }));
					  vh.app.use(vh.express.methodOverride());
					  vh.app.use(vh.express.cookieParser());
					  vh.app.use(vh.express.session({store:vh.store, secret:"jhtv,tbhleufvbufrfhbrbxerb32167"}));
					  vh.app.use(vh.express.static(__dirname + '/public'));
					});
					if(this.events.start && this.events.start instanceof Array){
						this.events.start.forEach(function(start){ start(vh) })
					}
					return this.app;
				}
		}
VirtualHost.prototype = ViHost;
module.exports = (function(config){
	var virtualhost = new VirtualHost(config);
	return virtualhost;
})()
