var vhost = require('./web.app.js')

vhost.on('start', function(vhost){
	
	var db = vhost.db({name:vhost.host})
		, fs = vhost.fs
		, log = vhost.sys.log
		, email = vhost.email.connect({
				user:"site@sochidetox.ru",
				password:'gjxnjdsqhj,jn',
				host:'smtp.yandex.ru',
				ssl:true
			})
	vhost.pages = db.collection('pages');
	
	vhost.app.get('/*', function(req, res, next){
		var template = require('./views/tpl.jsm').template
			, page = req.params[0] || 'index';
		template.vhost = vhost;
		console.log('>>>>>>>>>>>' + page)
		fs.exists(__dirname+'/views/'+page+'.jsm', function (exists) {
			page = exists ? page : '404'
		  log(req.connection.remoteAddress+" - [GET] : "+page);
		  vhost.pages.findOne({page:'/'+page},function(err, pagedata){
		  	res.send("<!DOCTYPE HTML>"+vhost.parse(template.get(page, pagedata)));
		  })
		  delete require.cache[__dirname+'/views/tpl.jsm'];
		})
	});
	
	vhost.wsocket(function(soc){	
		vhost.soc = soc;
		function login(auth){
			if(auth){
				var adminpanel = require(__dirname+'/views/adminpanel.jsm')
					, data = {
						auth:true, 
						panel:vhost.parse(adminpanel.content),
						init:adminpanel.jstemplate(adminpanel.script, auth)
					}
				delete require.cache[__dirname+'/views/adminpanel.jsm'];
				return data;
			} else {
				return {auth:false}
			}
		}

		vhost.soc.on('save', function(page){
			console.dir(['save page', page])
			page.name = page.name == '/' ? '/index' : page.name;
			vhost.pages.update({page:page.name},{$set:page},{upsert:true, safe:true},function(err,d){
				vhost.soc.emit('save', {page:page.name})
			})
		});
		vhost.soc.on('init', function(key){
			vhost.soc.session.get(function(ses){
				if(vhost.soc.handshake.session){
					if(ses.siodata){
						if(ses.siodata.loggedin){
							//console.dir(['logged in', ses.siodata])
							vhost.soc.emit('login', login({login:ses.siodata.login}))
						}
						vhost.soc.emit('init', ses.siodata)
					} else {
						//console.dir(['login fail', ses.siodata])
						vhost.soc.emit('init', {loggedin:false})
					}
				}
			})
		});
		vhost.soc.on('login', function(key){
			if(key.login != 'admin' || key.passw != 'bpvtytybt'){
				vhost.soc.emit('login', login(false))
			} else {
				vhost.soc.session.get(function(ses){
					ses.siodata = ses.siodata || {};
					ses.siodata.loggedin = true;
					ses.siodata.login = key.login;
					vhost.soc.session.set(ses._id, ses.siodata, function(err, r){
						if(!err && r) {
							console.log('[ '+ses._id+' ] - session updated')
							vhost.soc.emit('login', login({login:key.login}))
						} else {
							vhost.soc.emit('login', login(false))
						}
					})
				});
			}
		});
		vhost.soc.on('logout', function(key){
			vhost.soc.session.get(function(ses){
				ses.siodata = ses.siodata || {};
				ses.siodata.loggedin = false;
				ses.siodata.login = key.login;
				vhost.soc.session.set(ses._id, ses.siodata, function(err, r){
					if(!err && r) {
						console.log('[ '+ses._id+' ] - session updated')
						vhost.soc.emit('logout', login({login:key.login}))
					} else {
						vhost.soc.emit('logout', false)
					}
				})
			})
		});
		vhost.soc.on('order', function(order){
			email.send({
				text:order.phone,
				from:"site@sochidetox.ru",
				to:"admin@sochidetox.ru",
				subject:"Заявка на консультацию",
				attachment: [
			      {data: vhost.parse( 
			      	['html', null ,
				      	['p', {style:'margin-top:0px;font:18px arial'},"<b>Имя: </b>",order.name],
				      	['p', {style:'margin-top:0;margin-bottom:0;font:18px arial'},"<b>Тел: </b>",order.phone],
				      	"<br><hr><br>",
				      	['p', {style:'margin-top:0;font:18px arial'},"<b>Техт заявки:</b>"],
				      	['div', {style:'right:0px;margin-right:15px;margin-top:0;padding:15px;border:1px solid #000;font:16px arial;border-radius:6px;'},order.message]
				      ]), 
				      alternative:true
			      }
			   ]
			})
		});

		vhost.soc.on('reserved', function(reserve){
			var atach = vhost.parse(
			['html', null ,
				['p', {style:'margin-top:0px;font:18px arial'},"<b>Имя: </b>",reserve.data.fname+''],
				['p', {style:'margin-top:0px;font:18px arial'},"<b>Фамилия: </b>",reserve.data.lname+''],
				['p', {style:'margin-top:0px;font:18px arial'},"<b>дата заезда: </b>",reserve.data.data+''],
				['p', {style:'margin-top:0px;font:18px arial'},"<b>количество дней: </b>",reserve.data.days+''],
				['p', {style:'margin-top:0px;font:18px arial'},"<b>количество человек: </b>",reserve.data.persons+''],
				['p', {style:'margin-top:0px;font:18px arial'},"<b>количество номеров: </b>",reserve.data.apparts+''],
				"<br><hr><br>",
				['p', {style:'margin-top:0;margin-bottom:0;font:18px arial'},"<b>Тел: </b>",reserve.data.phone+''],
				['p', {style:'margin-top:0;margin-bottom:0;font:18px arial'},"<b>e-mail: </b>",reserve.data.mail+''],
				['p', {style:'margin-top:0;font:18px arial'},"<b>дополнительная информация:</b>"],
				['div', {style:'right:0px;margin-right:15px;margin-top:0;padding:15px;border:1px solid #000;font:16px arial;border-radius:6px;'},reserve.data.description+'']
			])
			email.send({
				text:reserve.data,
				from:"site@sochidetox.ru",
				to:"alibekova@wilstream.ru",
				subject:"Бронирование",
				attachment: [
			      {data: atach,
				      alternative:true
			      }
			   ]
			})
		});

		vhost.soc.on('question', function(question){
			var atach = vhost.parse(
			['html', null ,
				['p', {style:'margin-top:0;margin-bottom:0;font:18px arial'},"<b>e-mail: </b>",question.data.mail+''],
				['p', {style:'margin-top:0;font:18px arial'},"<b>дополнительная информация:</b>"],
				['div', {style:'right:0px;margin-right:15px;margin-top:0;padding:15px;border:1px solid #000;font:16px arial;border-radius:6px;'},question.data.question+'']
			])
			email.send({
				text:question.data,
				from:"site@sochidetox.ru",
				to:"alibekova@wilstream.ru",
				subject:"Вопрос посетителя",
				attachment: [
			      {data: atach,
				      alternative:true
			      }
			   ]
			})
		});

//		vhost.soc.on('getimages', function(page){
//			var images={};
//			r = execFile('find',['./public/img','-iname','*.png'],function(er,so,se){
//				so.split('\n').join('').split('/public/').forEach(function(img){
//					var image = img.split('/'), name = image.pop(),
//					p = images;
//					for(idx = 0; idx < image.length; idx++){
//						p[image[idx]] = p[image[idx]] || [];
//						p[image[idx]].push({image[idx]:[]});
//						p = p[image[idx]];						
//					};
//					p.push(name);
//				});
//			});
//		})
	})
	
	log(vhost.host + ' starting')
})
module.exports = vhost