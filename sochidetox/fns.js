function login(auth){
if(auth){
var adminpanel = require(process.cwd()+'/views/adminpanel.jsm');
return {
auth:true, 
panel:adminpanel.content,
init:(function(){
var rg = new RegExp(/::[^:]*::/gm), jstpl = [];
while((s = rg.exec(adminpanel.script)) != null){ jstpl[s[0]]=0 }
for(tpl in jstpl){
rg = new RegExp(tpl, 'gm')
adminpanel.script = adminpanel.script.replace(rg, eval(tpl.split('::').join('')))
}
return adminpanel.script
})()
}
} else {
return {auth:false}
}
}

function toString(o){
if(typeof o == 'string') return o;
}

function toString(o){
if(o instanceof Array) {
var s='[';
for(e=0; e<o.length; e++){
if(s!='[')s+=',';
s+=toString(o[e]) 
}
return s+']';
} else if(typeof o == 'function') {
return o.toString();
} else if( (typeof o == 'string') || (typeof o == 'number') || (typeof o == 'boolean') ) {
return '"'+o.toString()+'"';
} else {
var s='{'
for(f in o){
if(s != '{') s+=',';
s+='"'+f+'"'+':'+toString(o[f])
}
return s+'}'
}
}
